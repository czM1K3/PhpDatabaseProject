<?php
require_once "_includes/bootstrap.inc.php";
$m = new MustacheRunner();
$getter = new GetData();
$result = $getter->GetAllRooms($sortnum);

echo $m->render("head", array("title" => "Seznam místností"));
echo is_array($result) ? $m->render("room-list", $result) : $result;
echo $m->render("foot", array("back" => "index.html"));