<?php
//require_once "_includes/DatabaseConnector.class.php";
define("ERROR_MESSAGE", "Záznam neobsahuje žádná data");

class GetData
{
    private DatabaseConnector $dbConn;

    public function __construct()
    {
        $this->dbConn = new DatabaseConnector();
    }

    public function GetAllEmployees(&$sort): array|string
    {
        $sort = filter_input(INPUT_GET, 'sort', FILTER_VALIDATE_INT, array("options" => array("default" => 0, "min_range" => 0, "max_range" => 7)));
        $stmt = $this->dbConn->GetAllEmployees($sort);
        return $this->CheckAllData($stmt, $sort);
    }

    public function GetAllRooms(&$sort): array|string
    {
        $sort = filter_input(INPUT_GET, 'sort', FILTER_VALIDATE_INT, array("options" => array("default" => 0, "min_range" => 0, "max_range" => 5)));
        $stmt = $this->dbConn->GetAllRooms($sort);
        return $this->CheckAllData($stmt, $sort);
    }

    public function GetRoom(): array|string
    {
        $id = filter_input(1, "roomId", FILTER_VALIDATE_INT);
        if (!$id) return $this->Error();
        $result = $this->dbConn->GetRoom($id);
        if (!$result) return $this->Error();
        if (is_string($result)) return $result;
        $result["employees"] = $this->dbConn->GetRoomEmployees($id);
        $result["salary"] = floatval($this->dbConn->GetRoomSalary($id)["avg"]);
        $result["keys"] = $this->dbConn->GetRoomKeys($id);

        return $result;
    }

    public function GetEmployee(): array|string
    {
        $id = filter_input(1, "employeeId", FILTER_VALIDATE_INT);
        if (!$id) return $this->Error();
        $result = $this->dbConn->GetEmployee($id);
        if (!$result) return $this->Error();
        if (is_string($result)) return $result;
        $result["keys"] = $this->dbConn->GetEmployeeKeys($id);
        $result["firstletter"] = $result["firstname"][0];

        return $result;
    }


    private function CheckAllData($stmt, $sort): array|string
    {
        $result = array();
        if ($stmt == false) {
            $result = $this->Error();
        } else {
            while ($row = $stmt->fetch()) {
                array_push($result, $row);
            }
            $result = array("items" => $result);
            $result["sort-" . $sort] = true;

        }
        return $result;
    }

    private function Error(): string
    {
        http_response_code(404);
        return ERROR_MESSAGE;
    }
}