<?php


class DatabaseConnector
{
    private PDO $pdo;

    public function __construct(){
        $config = parse_ini_file("database.ini");
        $dsn = "mysql:host=".$config['HOST'].";dbname=".$config['DBNAME'].";charset=".$config['CHARSET'];

        $options = [
            PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION,
            PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC,
            PDO::ATTR_EMULATE_PREPARES => false,
        ];
        $this->pdo = new PDO($dsn, $config['DBUSER'], $config['DBPASS'], $options);
    }

    public function GetAllRooms($sort): bool|PDOStatement
    {
        $stmt = $this->pdo->query('SELECT * FROM room order by '. ($sort <= 1 ? 'name' : ($sort <= 3 ? 'no' : 'phone')) . ' '. ($sort % 2 === 0 ? 'asc': 'desc'));
        return $stmt->rowCount() == 1 ? false : $stmt;
    }

    public function GetAllEmployees($sort): bool|PDOStatement
    {
        $stmt = $this->pdo->query('select e.employee_id, concat(e.name, concat(" ", e.surname)) as name, e.job as position, r.name as room_name, r.phone from employee e join room r where e.room = r.room_id order by '. ($sort <= 1 ? 'name' : ($sort <= 3 ? 'position' : ($sort <= 5 ? 'phone':'room_name'))) . ' '. ($sort % 2 === 0 ? 'asc': 'desc'));
        return $stmt->rowCount() == 1 ? false : $stmt;
    }

    public function GetRoom($id): bool|string|array
    {
        $stmt = $this->pdo->prepare("SELECT * FROM room WHERE room_id=:id");
        $stmt->execute(['id' => $id]);
        if ($stmt->rowCount() == 0) return false;
        $fetched = $stmt->fetch();
        unset($stmt);
        return $fetched;
    }

    public function GetEmployee($id): bool|string|array
    {
        $stmt = $this->pdo->prepare("select e.name as firstname, e.surname, e.job, e.wage, r.room_id, r.name as room_name from employee
e join room r where e.room = r.room_id and e.employee_id=:id");
        $stmt->execute(['id' => $id]);
        if ($stmt->rowCount() == 0) return false;
        $fetched = $stmt->fetch();
        unset($stmt);
        return $fetched;
    }

    public function GetRoomEmployees($id): bool|array
    {
        $stmt = $this->pdo->prepare("select concat(e.name, concat(' ', e.surname)) as name, e.employee_id as id from employee e join room r where e.room = r.room_id and r.room_id = :id");
        $stmt->execute(['id' => $id]);
        if ($stmt->rowCount() == 0) return false;
        $result = array();
        while ($row = $stmt->fetch()) {
            array_push($result, $row);
        }
        unset($stmt);
        return $result;
    }

    public function GetRoomSalary($id){
        $stmt = $this->pdo->prepare("select avg(e.wage) as 'avg' from employee e join room r where e.room = r.room_id and r.room_id = :id");
        $stmt->execute(['id' => $id]);
        if ($stmt->rowCount() == 0) return false;
        $fetched = $stmt->fetch();
        unset($stmt);
        return $fetched;
    }

    public function GetEmployeeKeys($id): bool|array
    {
        $stmt = $this->pdo->prepare("select r.name, r.room_id from `key` k join employee e join room r where e.employee_id=k.employee and r.room_id=k.room and e.employee_id=:id");
        $stmt->execute(['id' => $id]);
        if ($stmt->rowCount() == 0) return false;
        $fetched = $stmt->fetchAll();
        unset($stmt);
        return $fetched;
    }

    public function GetRoomKeys($id): bool|array
    {
        $stmt = $this->pdo->prepare("select concat(e.name, concat(' ', e.surname)) as name, e.employee_id as id from employee e join `key` as k where e.employee_id = k.employee and k.room = :id");
        $stmt->execute(['id' => $id]);
        if ($stmt->rowCount() == 0) return false;
        $result = array();
        while ($row = $stmt->fetch()) {
            array_push($result, $row);
        }
        unset($stmt);
        return $result;
    }
}