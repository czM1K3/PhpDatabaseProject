<?php

mb_internal_encoding("UTF-8");

require_once __DIR__ . "/../vendor/autoload.php";

use Tracy\Debugger;
Debugger::enable();

spl_autoload_register(function ($className){
    include __DIR__ . '/' . $className . '.class.php';
});