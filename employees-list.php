<?php
require_once "_includes/bootstrap.inc.php";
$m = new MustacheRunner();
$getter = new GetData();
$result = $getter->GetAllEmployees($sortnum);

echo $m->render("head", array("title" => "Seznam zaměstnanců"));
echo is_array($result) ? $m->render("employees-list", $result) : $result;
echo $m->render("foot", array("back" => "index.html"));