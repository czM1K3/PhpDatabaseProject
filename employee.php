<?php
require_once "_includes/bootstrap.inc.php";
$m = new MustacheRunner();
$getter = new GetData();
$result = $getter->GetEmployee();

echo $m->render("head", array("title" => (is_array($result) ? "Karta osoby: " . htmlspecialchars($result["surname"]) . " " . htmlspecialchars($result["firstletter"]) . "." : "Chyba")));
echo is_array($result) ? $m->render("employee", $result) : $result;
echo $m->render("foot", array("back" => "employees-list.php"));