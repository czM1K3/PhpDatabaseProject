<?php
require_once "_includes/bootstrap.inc.php";
$m = new MustacheRunner();
$getter = new GetData();
$result = $getter->GetRoom();

echo $m->render("head", array("title" => (is_array($result) ? "Místnost " . htmlspecialchars($result["no"]) : "Chyba")));
echo is_array($result) ? $m->render("room", $result) : $result;
echo $m->render("foot", array("back" => "room-list.php"));